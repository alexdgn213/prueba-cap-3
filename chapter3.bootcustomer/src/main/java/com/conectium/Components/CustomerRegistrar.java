package com.conectium.Components;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import com.conectium.Entities.Customer;
import com.conectium.Repositories.CustomerRespository;

@Component
@Lazy
public
class CustomerRegistrar {
CustomerRespository customerRespository;
Sender sender;
@Autowired
CustomerRegistrar(CustomerRespository customerRespository,
Sender sender){
this.customerRespository = customerRespository;
this.sender = sender;
}
// ideally repository will return a Mono object
public Customer register(Customer customer){
if(customerRespository.findByName(
customer.getName()).isPresent())
System.out.println("Duplicate Customer./nNo Action required");
else {
customerRespository.save(customer);
sender.send(customer.getEmail());
}
return customer; //HARD CODED BECOSE THERE IS NO REACTIVE REPOSITORY.
}
}