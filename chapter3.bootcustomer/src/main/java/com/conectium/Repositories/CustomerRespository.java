package com.conectium.Repositories;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import com.conectium.Entities.*;

@RepositoryRestResource
public interface CustomerRespository extends JpaRepository
<Customer,Long>{
	Optional<Customer> findByName(@Param("name") String name);
}
